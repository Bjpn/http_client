#!/usr/bin/env python
import tornado.ioloop
import tornado.web
import sys

# Handlers imports

# User handler imports
from handlers.user.auth.LoginHandler import *
from handlers.user.auth.LogoutHandler import *
from handlers.user.auth.RegisterHandler import *


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")

application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/login", LoginHandler),
    (r"/logout", LogoutHandler),
    (r"/register", RegisterHandler),
])

def main():
    if len(sys.argv) < 2:
        port = 5000
    else:
        port = int(sys.argv[1])
    application.listen(port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()
