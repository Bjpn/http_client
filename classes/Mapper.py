from pymongo import MongoClient
from bson.objectid import ObjectId
class Mapper:
    """
    Klasa mapujaca obiekty do bazy'

    Kazda klasa dziedziczaca musi sie stosowac do nastepujacych uwag
    1. Implementuje serialize (with sanitaze), deserialize (with unsanitaze)
    2. Wywoluje w konstruktorze konstruktor rodzica
    3. Podczas serializacji zwraca _id jako ObjectId (_id jest niezmienialne!!!) [moze byc puste przy dodawaniu]
    4. Posiada atrybut table, w ktorym zawiera sie nazwa kolekcji do jakiej jest przypisany
    """

    table = ""
    database = 'test'
    _id = ""

    def __init__(self):
        client = MongoClient('localhost', 27017)
        db = client[self.database]
        self.collection = db[self.table]

    def setId(self, id):
        self._id = ObjectId(id)

    def serialize(self):
        """Serializacja obiektu dziecka (do zaimplementowania)"""
        raise NotImplementedError("Please Implement this method")

    def deserialize(self, json, cyclic):
        raise NotImplementedError("Please Implement this method")

    def update(self):
        """Aktualizuje istniejacy obiekt dziecko"""
        _object = self.serialize()
        _objectId = ObjectId(_object['_id'])
        _object.pop('_id', None)
        self.collection.update({'_id': _objectId}, {'$set': _object})

    def add(self):
        """Dodaje nowy obiekt dziecko"""
        _object = self.serialize()
        _object.pop('_id', None)
        self._id = ObjectId(self.collection.insert(_object))

    def load(self, cyclic=False):
        _objectId = ObjectId(self._id)
        if self.collection.find({'_id': _objectId}).count():
            self.deserialize(self.collection.find_one({'_id': _objectId}), cyclic)

    def loadBy(self, json):
        self.deserialize(self.collection.find_one(json))

    def countBy(self, json):

        return self.collection.find(json).count()
