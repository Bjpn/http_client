import re, hashlib, random, datetime
import classes.Mapper
from bson.objectid import ObjectId
from bson import json_util

# Dodatkowe klasy
from classes.Image import Image

class User(classes.Mapper.Mapper):
    'Klasa uzytkownika'

    table = 'users'

    _id = ""
    email = ""
    password = "" # przechowywane w zahashowanej postaci
    salt = "" # przyslowiowa sol do hasla
    authKey = ""
    authDate = datetime.datetime(1900, 1, 1)
    active = False
    online = False
    nickname = ''
    # location =
    # TODO Klasa Location
    avatar = Image()
    aboutMe = ""
    birthday = datetime.datetime(1900, 1, 1)
    achievements = []
    followers = []
    tracking = []

    def __init__(self, json=None):
        classes.Mapper.Mapper.__init__(self)
        if json:
            self.deserialize(json)

    def login(self, password):
        hasher = hashlib.sha256()
        hasher.update(password + self.salt)
        hashed_password = hasher.hexdigest()
        if (self.password == hashed_password and self.active):
            self.online = True
            self.generateKey()
            self.authDate = datetime.datetime.now()
            return True

        self.online = False
        return False

    def logout(self):
        self.online = False

    def auth(self, key):
        # Polityka odnowienia klucza na 48h
        diff = datetime.datetime.now() - self.authDate
        if diff > datetime.timedelta(hours=48):
            return False
        if self.authKey == key:
            return True
        else:
            return False

    def serialize(self):
        json = {'_id': str(self._id), 'email': self.email, 'password': self.password, 'salt': self.salt, 'authKey': self.authKey, 'authDate': self.authDate, 'active': self.active, 'online': self.online, 'nickname': self.nickname, 'avatar': self.avatar.serialize(), 'aboutMe': self.aboutMe, 'birthday': {'year': self.birthday.year, 'month': self.birthday.month, 'day': self.birthday.day}}
        return json

    def generateSalt(self):
        hasher = hashlib.sha256()
        salt = str(random.random()) + self.email + str(datetime.date.today())
        hasher.update(salt)
        self.salt = hasher.hexdigest()

    def generateKey(self):
        hasher = hashlib.sha256()
        key = str(random.random()) + self.email + str(random.random()) + self.password + str(datetime.date.today())
        hasher.update(key)
        self.authKey = hasher.hexdigest()

    def deserialize(self, json):
        self._id = json['_id']
        self.email = json['email']
        self.password = json['password']
        self.salt = json['salt']
        self.authKey = json['authKey']
        self.authDate = json['authDate']
        self.active = json['active']
        self.online = json['online']
        self.aboutMe = json['aboutMe']
        self.nickname = json['nickname']
        self.birthday = datetime.date(int(json['birthday']['year']), int(json['birthday']['month']), int(json['birthday']['day']))


    def getEmail(self):
        return self.email

    def setEmail(self, email):
        if re.match(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", email):
            self.email = email
        else:
            raise 'Bad email'
        # TODO Ulepszyc sprawdzanie maila (dodac ogolny InputError)

    def getPassword(self):
        return self.password

    def setPassword(self, open_pass):
        # Polityka sprawdzania hasla
        # TODO InputError

        # minimalna dlugosc 5 znakow
        if re.match(r"\S{5,}", open_pass):
            pass
        #else:
            #raise "Too short password"

        # musza byc litery
        if re.match(r"\w+", open_pass):
            pass
        #else:
            #raise "Must contains letter"

        hasher = hashlib.sha256()
        self.generateSalt()
        hasher.update(open_pass + self.salt)
        self.password = hasher.hexdigest()

    def setNickname(self, nickname):
        if len(nickname) <=32:
            self.nickname = nickname
        else:
            raise "Bad nickname"