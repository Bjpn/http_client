import base64, hashlib, os

class Image:
    """
    Klasa obracajaca obrazkami

    Obrazki na 99 procent beda dostarczane nginxem lub apachem
    """

    server = "http://localhost:8080/"
    image_place = "images/"
    path = ""
    name = ""

    def __init__(self, json=None):
        if json:
            self.deserialize(json)

    def getPath(self):
        return self.server + self.path

    def serialize(self):
        json = {'class': 'Image', 'name': self.name, 'path': self.getPath()}
        return json

    def deserialize(self, json):
        if json.has_key('path'):
            self.path = json['path']
        if json.has_key('name'):
            self.name = json['name']

    def deleteImage(self):
        if self.path:
            try:
                os.remove(self.path)
            except OSError:
                pass

    def saveImage(self, base64_image, path_prefix, prefix):
        self.deleteImage()
        data = base64.b64decode(base64_image)
        hasher = hashlib.sha256()
        hasher.update(base64_image)
        self.path = self.image_place + path_prefix + prefix + '_' + hasher.hexdigest()
        f = open(self.path, 'w')
        f.write(data)
        f.close()

    def getImage(self):
        f = open(self.path, 'r')
        data = f.read()
        base64_image = base64.b64encode(data)
        return base64_image
