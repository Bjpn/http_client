import tornado.web, json

class BaseHandler(tornado.web.RequestHandler):
    'Podstawowy handler, wszystkie musza po nim dziedziczyc'

    database = 'test'

    def post(self):
        request = json.loads(self.request.body)
        self.set_header("Content-Type", "application/json")

    def badRequest(self):
        #self.set_status(500)
        response = json.dumps({'code': 200, 'status': 'Bad request'})
        self.finish(response)

    def authError(self):
        #self.set_status(403)
        response = json.dumps({'code': 300, 'status': 'Authentication error'})
        self.finish(response)
