import tornado.web
import json
from classes.User import User
from pymongo import MongoClient
import handlers.BaseHandler


class RegisterHandler(handlers.BaseHandler.BaseHandler):

    database = 'test'

    def post(self):
        request = json.loads(self.request.body)
        self.set_header("Content-Type", "application/json")

        if request['status'] != 'register':
            self.badRequest()
            return
        client = MongoClient('localhost', 27017)
        db = client[self.database]

        users = db.users

        # uzupelniamu tutaj klase
        try:
            nickname = request['nickname']
            email = request['email']
            password = request['password']
        except KeyError as e:
            # print e.message
            self.badRequest()
            return
        user = User()
        if user.countBy({'email': email}) != 0:
            self.badEmail()
            return
        user.setEmail(email)
        user.setPassword(password)
        user.nickname = nickname
        user.active = True

        # Serialize zmienia z normalnych argumentow na json deserialize robi odwrotnie
        user.add()
        #Odpisanie apce


        response = json.dumps({'code':100, 'status': 'Good registration'})
        self.finish(response)



    def badEmail(self):
        # self.set_status(500)
        response = json.dumps({'code': 200, 'status': 'Mail exists'})
        self.finish(response)