import tornado.web
import json
from pymongo import MongoClient
from classes.User import User
import handlers.BaseHandler

class LogoutHandler(handlers.BaseHandler.BaseHandler):

    def post(self):
        request = json.loads(self.request.body)
        self.set_header("Content-Type", "application/json")
        try:
            if request['status'] != 'logout':
                self.badRequest()
                return

            client = MongoClient('localhost', 27017)
            db = client[self.database]
            users = db.users
            r_num = users.find({'email': request['email'], 'active': True}).count()
            if r_num == 0:
                self.authError()
                return

            key = request["key"]
            user = User()
            user.loadBy({'email': request['email'], 'active': True})
            if not user.auth(key):
                self.authError()
                return

            user.logout()
            user.update()
            response = json.dumps({'code': 100, 'status': 'Logout succesful'})
            self.finish(response)

        except KeyError as e:
            #print e.message
            self.badRequest()
            return #
