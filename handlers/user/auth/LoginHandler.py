import tornado.web
import json
from pymongo import MongoClient
from classes.User import User
import handlers.BaseHandler

class LoginHandler(handlers.BaseHandler.BaseHandler):

    def post(self):
        request = json.loads(self.request.body)
        self.set_header("Content-Type", "application/json")
        if request['status'] != 'login':
            self.badRequest()
            return

        client = MongoClient('localhost', 27017)
        db = client[self.database]
        users = db.users
        r_num = users.find({'email': request['email'], 'active': True}).count()
        if r_num == 0:
            self.authError()
            return

        user = User()
        user.loadBy({'email': request['email']})
        if user.login(request['password']):
            user.generateKey()
            response = json.dumps({'code': 100, 'status': 'Login succesful', 'key': user.authKey})
            user.update()
            self.finish(response)
        else:
            self.authError()
            return